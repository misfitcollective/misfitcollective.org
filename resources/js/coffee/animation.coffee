$ ->

	window.misfits.animation =

			text:

				displayLogo: ->
					@sequentialType('div#logo', 10, 200)

				displayTagline: ->
					@scatterFadeType('div#tagline', 2500, 1000)

				displaySequentialType: ->
					@sequentialType('div.sequential-type', 10, 200, 3000)

				displayCurves: ->
					$('div#curves span').hide()
					@sequentialFadeIn($('div#curves span'), 50, 2000)

				scatterFadeType: (selector, maxDelay, fadeLength) ->
					@splitContentIntoContainers(selector)
					@scatterFadeIn(selector + ' div.word span', maxDelay, fadeLength)

				sequentialType: (selector, minDelay, maxDelay, startDelay) ->
					@splitContentIntoContainers(selector)
					if startDelay
						setTimeout =>
							@sequentialShow(selector + ' div.word span', minDelay, maxDelay)
						, startDelay
					else
						@sequentialShow(selector + ' div.word span', minDelay, maxDelay)

				splitContentIntoContainers: (selector) ->
					words = $(selector).text().split('|')
					$(selector).html('')
					$.each words, ->
						$(selector).append(misfits.animation.text.getWordDivTag(this))
					$(selector + ' div.word').each ->
						wordContainer = this
						letters = $(wordContainer).text().split('')
						$(wordContainer).html('')
						$(wordContainer).hide()
						$.each letters, ->
							thisLetter = letters.shift()
							fontSizeProperty = $(selector).css('font-size')
							letterContainer = misfits.animation.text.getLetterSpanTag(thisLetter, fontSizeProperty)
							$(wordContainer).append(letterContainer)
					$('div.word').each ->
						wordWidth = misfits.animation.text.getWordWidth(this)
						$(this)
							.width(wordWidth)
							.show()
					letterContainers = $(selector + ' div.word span')
					$(letterContainers).hide()
					@scaleTextToDisplay()

				getWordWidth: (element) ->
					totalWidth = 0
					$(element).children('span.type').each ->
						# letterWidth = parseInt($(this).css('width').replace(/\D/g,''))
						letterWidth = $(this).outerWidth()
						totalWidth += letterWidth
					# I have no idea why this is necessary but if it's not there the content will wrap
					totalWidth += totalWidth / 10
					return totalWidth

				getWordDivTag: (word) ->
					return '<div class="word">' + word + '</div>'

				getLetterSpanTag: (letter, fontSizeProperty) ->
					fontSize = fontSizeProperty.replace(/\D/g,'')
					fontUnits = fontSizeProperty.replace(/[0-9]/g, '')
					marginTopCss = @getMarginTopCss(fontSize, fontUnits)
					widthCss = @getWidthCss(fontSize, fontUnits)
					styleAttribute = 'style="' + marginTopCss + widthCss + '"'
					return '<span class="type" ' + styleAttribute + '">' + letter + '</span>'

				getMarginTopCss: (fontSize, fontUnits) ->
					maxMargin = fontSize / 20
					marginTop = misfits.math.random(0, maxMargin)
					return 'margin-top: ' + marginTop + fontUnits + ';'

				getWidthCss: (fontSize, fontUnits) ->
					minWidth = fontSize / 3.5
					maxWidth = fontSize / 2
					width = misfits.math.random(minWidth, maxWidth)
					return 'width: ' + width + fontUnits + ';'

				scatterFadeIn: (selector, maxDelay, fadeLength) ->
					$(selector).each ->
						element = $(this)
						delay = misfits.math.random(0, maxDelay)
						setTimeout ->
							element.fadeIn(fadeLength)
						, delay

				sequentialFadeIn: (selector, interval, fadeLength) ->
					totalInterval = 0
					$(selector).each ->
						element = $(this)
						totalInterval += interval
						setTimeout ->
							element.fadeIn(fadeLength)
						, totalInterval

				sequentialShow: (selector, minDelay, maxDelay) ->
					totalDelay = 0
					$(selector).each ->
						element = $(this)
						delay = misfits.math.random(minDelay, maxDelay)
						totalDelay += delay
						setTimeout ->
							element.show()
						, totalDelay

				scaleTextToDisplay: ->
					largestDivWidth = 0;
					$('div.word').each ->
						largestDivWidth = $(this).width() if $(this).width() > largestDivWidth
					screenWidth = $(window).width()
					if largestDivWidth > screenWidth
						scaleRatio = screenWidth / largestDivWidth
						$('div.word').css('transform', 'scale(' + scaleRatio + ')')

			color:

				darkestColor: [20, 20, 20]
				brightestColor: [90, 30, 90]
				interval: 20000
				lightColorIncrement: 40
				darkColorIncrement: -40

				cycle: ->
					setInterval =>
						@doCycle()
					, @interval

				doCycle: ->
					baseColor = @getRandomRGB(@darkestColor, @brightestColor)
					lightColor = @incrementColor(baseColor, @lightColorIncrement)
					darkColor = @incrementColor(baseColor, @darkColorIncrement)
					@fadeBackgroundColor('.cycleBackgroundColor', baseColor, @interval)
					@fadeColor('.cycleColorLight', lightColor, @interval)
					@fadeColor('.cycleColorDark', darkColor, @interval)

				fadeBackgroundColor: (selector, rgb, interval) ->
					color = @getRGBCSS(rgb)
					$(selector).animate({
						backgroundColor: color,
					}, interval)

				fadeColor: (selector, rgb, interval) ->
					color = @getRGBCSS(rgb)
					$(selector).animate({
						color: color
					}, interval)

				getRandomRGB: (@darkestColor, @brightestColor) ->
					red = misfits.math.random(@darkestColor[0], @brightestColor[0])
					green = misfits.math.random(@darkestColor[2], @brightestColor[1])
					blue = misfits.math.random(@darkestColor[1], @brightestColor[2])
					return [red, green, blue]

				getRGBCSS: (rgb) ->
					red = rgb[0]
					green = rgb[1]
					blue = rgb[2]
					return "rgb(" + red + ', ' + green + ', ' + blue + ')'

				incrementColor: (rgb, increment) ->
					red = @incrementValue(rgb[0], increment)
					green = @incrementValue(rgb[1], increment)
					blue = @incrementValue(rgb[2], increment)
					return [red, green, blue]

				incrementValue: (value, increment) ->
					return Math.max(value + increment, 0)

			scale:

				scaleTo: (element, scale) ->
					$(element).css(
						'transition': 'all 0.5s',
						'transform': 'scale(' + scale + ')'
					)

			page:

				showContent: (clickedElement) ->
					misfits.animation.scale.scaleTo('#header', 0.5)
					misfits.animation.scale.scaleTo('#side-menu', 0.5)
					$('#content').show()
					$('#content').children().hide()
					target = 'div#' + $(this).data('target')
					$(target).show()


	misfits.animation.text.displayLogo()
	misfits.animation.text.displayTagline()
	misfits.animation.text.displaySequentialType()
	misfits.animation.color.cycle()

	$('#content').hide()

	$('#side-menu li').click ->
		misfits.animation.page.showContent(this)