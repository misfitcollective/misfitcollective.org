misfits.view =

	loadPartial: (container, fileName, callback) ->
		filePath = @getPartialPath(fileName)
		successCallback = (contents) -> 
			container.append(contents)
			callback()
		$.get(filePath, successCallback).fail(@logError)

	getPartialPath: (fileName) ->
		partialPath = '/views/partials/'
		fileExtension = '.html'
		return partialPath + fileName + fileExtension

	logError: (jqxhr, textStatus, error) ->
		err = textStatus + ", " + error
		console.log err
		console.log jqxhr.responseText

