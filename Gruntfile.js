module.exports = function(grunt) {

  concatTasks = {};
  concatTasks['options'] = {
    separator: ''
  }

  watchTasks = {};
  watchTasks['css'] = {
    files: ['<%= dirs.src_css %>/*.css'],
    tasks: ['concat:css', 'cssmin']
  }
  watchTasks['coffee'] = {
    files: ['<%= dirs.src_coffee %>/*.coffee'],
    tasks: ['coffee:compile', 'uglify']
  }
  watchTasks['qunit'] = {
    files: ['tests/qunit/coffee/*.coffee'],
    tasks: ['coffee:qunit']
  }
  watchTasks['js'] = {
    files: ['<%= dirs.src_js %>/*.js'],
    tasks: ['uglify']
  }
  concatTasks['css'] = {
    src: ['<%= dirs.src_css %>/*.css'],
    dest: '<%= dirs.dest %>/style.min.css'
  }

  // Automatically define tasks for qunit tests

  qunitTasks = {
    all: {
        options: {
          urls: []
        }
      }
  };

  grunt.file.recurse('tests/qunit/js/', function(abspath, rootdir, subdir, filename) {
    if (filename == '.keep') { return; }
    var testName = filename.replace("_test.js", "");
    var url = 'http://misfitcollective/tests/qunit/views/' + testName;
    // create task for this test
    qunitTasks[testName] = {
      options: {
        urls: [url]
      }
    }
    // add test to all
    qunitTasks['all']['options']['urls'].push(url);
  });

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    dirs: {
      src_css: "resources/css",
      src_js: "resources/js",
      src_coffee: "resources/js/coffee",
      dest_coffee: "resources/js/built",
      dest: "resources/dist"
    },
    concat: concatTasks,
    
    cssmin: {
      minify: {
        cwd: '<%= dirs.dest %>',
        expand: true,
        src: ['*.css', '!*.min.css'],
        dest: '<%= dirs.dest %>',
        ext: '.min.css'
      }
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        // beautify: {
        //   beautify: true,
        //   indent_level: 0
        // },
        mangle: false
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= dirs.src_js %>',
          src: '*.js',
          dest: '<%= dirs.dest %>'
        }]
      },
      vendor: {
        files: [{
          expand: true,
          cwd: '<%= dirs.src_js %>' + '/vendor/',
          src: '*.js',
          dest: '<%= dirs.dest %>' + '/vendor/'
        }]
      },
      built: {
        files: [{
          expand: true,
          cwd: '<%= dirs.dest_coffee %>',
          src: '*.js',
          dest: '<%= dirs.dest %>'
        }]
      }
    },

    coffee: {
      compile: {
        expand: true,
        flatten: true,
        cwd: '<%= dirs.src_coffee %>',
        src: ['*.coffee'],
        dest: '<%= dirs.dest_coffee %>',
        ext: '.js'
      },
      qunit: {
        expand: true,
        flatten: true,
        cwd: 'app/webroot/qunit/coffee',
        src: ['*.coffee'],
        dest: 'app/webroot/qunit/js/built',
        ext: '.js'
      }
    },

    qunit: qunitTasks,
    watch: watchTasks

  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-qunit');

  grunt.registerTask('default', ['concat', 'coffee', 'cssmin', 'uglify']);
  grunt.registerTask('concatcss', 'concat:css');

};
