# MisfitCollective.org

## Project Description

This is the source for [misfitcollective.org](http://misfitcollective.org)

### The Technologies We Used

#### Server-side logic
None.  Pure html / css.

#### Presentation Code
This is where most of the fun happens.  Client-side stuff is handled by jQuery / HTML5, written in CoffeeScript and compiled via [Grunt](http://gruntjs.com/) with [grunt-contrib-coffee](https://github.com/gruntjs/grunt-contrib-coffee).  Stylesheets are CSS3.  Responsive design is handled via [Bootstrap](http://getbootstrap.com/).

#### Resource Pipeline
We use a fully-custom resource pipeline that provides raw resources in development, and minified resources in production.  Minification is handled with [grunt-contrib-cssmin](https://github.com/gruntjs/grunt-contrib-cssmin) for css and [grunt-contrib-uglify](https://github.com/gruntjs/grunt-contrib-uglify).

#### Package Management
We use Composer for PHP package management, NPM for server-side javascript libraries, and Bower for other external dependencies (css/js).

#### Testing
We lurv TDD/BDD. <3

Server-side PHP testing is handled with PHPUnit.  Javascript unit testing is QUnit, rigged up to allow CoffeeScript for the test cases, because we also lurv CS.  

No feature tests in this rig; it's not really complex enough to warrant the time.

## Installation

1. Clone the repo: `git clone git@bitbucket.org:misfitcollective/misfitcollective.org.git`
1. Install [node package manager (npm)](https://github.com/npm/npm) and run npm to install dependencies: `sudo npm install`
1. Life will be a little easier with global installs of Grunt and Bower, if you don't already have them.
		sudo npm install -g bower
		sudo npm install -g grunt
1. Install Bower dependencies: `bower install`